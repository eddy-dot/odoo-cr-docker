FROM odoo:12.0
MAINTAINER Ticpymes S.A. <dits.technology@gmail.com>

USER root

COPY l10n_cr /mnt/extra-addons
COPY config /etc/odoo

RUN pip3 install \        
        suds-py3 \
        PyPDF2 \
        passlib \
        werkzeug \
        python-dateutil \
        PyYAML \
        psycopg2 \
        psutil \
        image \
        jinja2 \
        reportlab \
        html2text \
        num2words \
        phonenumbers \
        vatnumber \
        xlsxwriter \
        jsonschema \
        xmlsig \
        pyopenssl \
        zeep

RUN python3 -m pip install paramiko

USER odoo